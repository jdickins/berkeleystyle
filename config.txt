lumi,#sqrt{#it{s}} = 13 TeV,79.8 fb^{-1}
xmin,-2.5
xmax,5.5

# example syntax for including a combined bin
#comb,Combined,nllscan/gg2H_0J/nllscan_r_gg2H_0J.txt,nllscan/gg2H_0J_stat/nllscan_r_gg2H_0J.txt,theory/gg2H_0J.txt

# example syntax for including an arrow for an upper limit
#lim,ggF 0J,nllscan/gg2H_0J/nllscan_r_gg2H_0J.txt,nllscan/gg2H_0J_stat/nllscan_r_gg2H_0J.txt,theory/gg2H_0J.txt

bin,ggF 0J,nllscan/gg2H_0J/nllscan_r_gg2H_0J.txt,nllscan/gg2H_0J_stat/nllscan_r_gg2H_0J.txt,theory/gg2H_0J.txt

bin,ggF 1J p_{T}^{H} < 60,nllscan/gg2H_1J_ptH_0_60/nllscan_r_gg2H_1J_ptH_0_60.txt,nllscan/gg2H_1J_ptH_0_60_stat/nllscan_r_gg2H_1J_ptH_0_60.txt,theory/gg2H_1J_ptH_0_60.txt

bin,ggF 1J 60 < p_{T}^{H} < 120,nllscan/gg2H_1J_ptH_60_120/nllscan_r_gg2H_1J_ptH_60_120.txt,nllscan/gg2H_1J_ptH_60_120_stat/nllscan_r_gg2H_1J_ptH_60_120.txt,theory/gg2H_1J_ptH_60_120.txt

bin,ggF 1J 120 < p_{T}^{H} < 200,nllscan/gg2H_1J_ptH_120_200/nllscan_r_gg2H_1J_ptH_120_200.txt,nllscan/gg2H_1J_ptH_120_200_stat/nllscan_r_gg2H_1J_ptH_120_200.txt,theory/gg2H_1J_ptH_120_200.txt

bin,ggF #geq 2J,nllscan/gg2H_ge2J/nllscan_r_gg2H_ge2J_ptH_0_200_VBFtopo.txt,nllscan/gg2H_ge2J_stat/nllscan_r_gg2H_ge2J_ptH_0_200_VBFtopo.txt,theory/gg2H_ge2J_ptH_0_200_VBFtopo.txt

bin,ggF BSM + qq2Hqq BSM,nllscan/gg2H_ge1J_ptH_gt200_plus_qq2Hqq_pTjet1_gt200/nllscan_r_gg2H_ge1J_ptH_gt200_plus_qq2Hqq_pTjet1_gt200.txt,nllscan/gg2H_ge1J_ptH_gt200_plus_qq2Hqq_pTjet1_gt200_stat/nllscan_r_gg2H_ge1J_ptH_gt200_plus_qq2Hqq_pTjet1_gt200.txt,theory/gg2H_ge1J_ptH_gt200_plus_qq2Hqq_pTjet1_gt200.txt

bin,VBF,nllscan/qq2Hqq/nllscan_r_qq2Hqq_pTjet1_0_200.txt,nllscan/qq2Hqq_stat/nllscan_r_qq2Hqq_pTjet1_0_200.txt,theory/qq2Hqq_pTjet1_0_200.txt

bin,VHlep,nllscan/VHlep/nllscan_r_VHlep.txt,nllscan/VHlep_stat/nllscan_r_VHlep.txt,theory/VHlep.txt

bin,top,nllscan/top/nllscan_r_ttH.txt,nllscan/top_stat/nllscan_r_ttH.txt,theory/ttH.txt

