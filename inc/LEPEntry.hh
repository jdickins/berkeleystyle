#include <iostream>
#include <stdlib.h>
#include <vector>
#include <map>

#include "TFile.h"
#include "TH1F.h"
#include "TString.h"
#include "THStack.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TMathText.h"
#include "TStyle.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TMarker.h"
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TLine.h"
#include "TBox.h"
#include "TPolyLine.h"

using namespace std;

class LEPError
{
  TString _name;
  
public:
  
  double lo;
  double hi;
  bool  asymm;
  int   status;  // 0:inactive; 1:in-quadrature; 2:linearly

  void setName( const char* name ) { _name=name; } 
  TString name() const { return _name; }
  double err() const 
  {
    if( status==0 ) return 0;
    if( asymm ) return 0.5*(lo+hi);
    return lo;
  }
  bool ok() const { return status>0; }
  double errlo() const 
  {
    if( status==0 ) return 0;
    return lo;
  }
  double errhi() const 
  {
    if( status==0 ) return 0;
    return hi;
  }
  
  LEPError( const char* name, double err=0 ) 
    : _name(name), lo(err), hi(err), asymm(false), status(0) {}
  LEPError( const char* name, double errlo, double errhi ) 
    : _name(name), lo(errlo), hi(errhi), asymm(true), status(0) {}
  ~LEPError() {}
  //  ClassDef(LEPError,1);
};

class LEPEntry
{
private:

  TString    _name;
  TString    _mathtext;
  TString    _unit;
  double        _val;
  LEPError _tot;
  LEPError _stat;
  LEPError _syst;
  LEPError _theo;
  LEPError _lumi;

  int    _ndigit;

  int     _marker;
  double      _size;
  int       _color;

  int     _txtfont;
  double   _txtsize;
  double _txtalign;

  double _x1, _y1;
  double _x2, _y2;
  double _x3, _y3;
  double _dy;

  bool    _writeResult;
  double   _scaleResult;
  int        _fontResult;
  int      _colorResult;

  double _lineWidth;

  double _boxSizeStat;
  double _boxSizeSyst;
  double _boxLineWidth;
  
public:

  LEPEntry(const char* name=0, const char* unit=0, const char* mathText=0)
    : _name(name), _mathtext(mathText), _unit(unit), _tot("tot"),_stat("stat"), _syst("syst"), _theo("theo"), _lumi("lumi"),
      _x1(0), _y1(0),
      _x2(0), _y2(0),
      _x3(0), _y3(0),
      _dy(0),
      _writeResult(false), _scaleResult(0.8), _lineWidth(2),
      _boxSizeStat(0.25), _boxSizeSyst(0.1), _boxLineWidth(1.2)
  {
    _val=0;
    setStyle();
    setNdigit();
  }
  ~LEPEntry(){};

  void setStyle( int marker=kFullCircle, double size=0.8, int color=kBlack,
                 int txtfont=42, double txtsize=0.8, int txtalign=11,
                 double lineWidth=2 ){
    _marker   = marker;
    _size     = size;
    _color    = color;
    _txtfont  = txtfont;
    _txtsize  = txtsize;
    _txtalign = txtalign;
    _lineWidth = lineWidth;
    _colorResult = _color;
    _fontResult = _txtfont;
  }

  TString name() { return _name; }
  void setVal( double val=0 ){ _val=val; }
  void setValAndErrors( double val=0, double stat=0, double syst=0,
                        double theo=0, double lumi=0.11 ){
    setVal(val);
    setStat(stat);
    setSyst(syst);
    setTheo(theo);
    setLumi(lumi);
  }
  
  void setTot( double tot ){
    _tot.asymm=false;
    _tot.lo=tot;
    _tot.hi=tot;
    _tot.status=1;
  }

  void setStat( double stat ){
    _stat.asymm=false;
    _stat.lo=stat;
    _stat.hi=stat;
    _stat.status=1;
  }

  void setSyst( double syst ){
    _syst.asymm=false;
    _syst.lo=syst;
    _syst.hi=syst;
    _syst.status=1;
  }

  void setListOfSyst( vector<double> syst ){
    double syst_(0);
    for( size_t ii=0; ii<syst.size(); ii++ )
      {
	syst_ += pow( syst[ii], 2 );
      }
    setSyst( sqrt( syst_ ) );
  }

  void setTheo( double theo ){
    _theo.asymm=false;
    _theo.lo=theo;
    _theo.hi=theo;
    _theo.status=1;
  }
  
  void setAsymmTot( double totlo, double tothi ){
    _tot.asymm=true;
    _tot.lo=totlo;
    _tot.hi=tothi;
    _tot.status=1;
  }
  
  void setAsymmStat( double statlo, double stathi ){
    _stat.asymm=true;
    _stat.lo=statlo;
    _stat.hi=stathi;
    _stat.status=1;
  }

  void setAsymmSyst( double systlo, double systhi ){
    _syst.asymm=true;
    _syst.lo=systlo;
    _syst.hi=systhi;
    _syst.status=1;
  }

  void setAsymmTheo( double theolo, double theohi ){
    _theo.asymm=true;
    _theo.lo=theolo;
    _theo.hi=theohi;
    _theo.status=1;
  }

  void setLumi( double lumi, bool relative=true ){
    double lumi_(lumi);
    if( relative ) lumi_*=_val;
    _lumi.asymm=false;
    _lumi.lo=lumi_;
    _lumi.hi=lumi_;
    _lumi.status=2;
  }

  void setNdigit( int ndigit=3 ) { _ndigit = ndigit; }
  void setResultStyle( int color=kBlack, int txtfont=42 ) { _colorResult = color; _fontResult = txtfont; }
  void setX( double x1, double x2=0 ) { _x1=x1; _x2=x2; }
  void setY( double y1, double y2=0 ) { _y1=y1; _y2=y2; }
  void setXYMath( double x3, double y3 ) { _x3=x3; _y3=y3; }
  void setDY( double dy ) { _dy=dy; }
  void setStatName( const char* name ) { _stat.setName(name); }
  void setSystName( const char* name ) { _syst.setName(name); }
  void setTheoName( const char* name ) { _theo.setName(name); }
  void setLumiName( const char* name ) { _lumi.setName(name); }
  void setScale( double scale ) { _scaleResult=scale; }
  void setStatBoxSize(double size) {_boxSizeStat=size;}
  void setSystBoxSize(double size) {_boxSizeSyst=size;}
  void setBoxLineWidth(double width) {_boxLineWidth=width;}
  void writeResult( int yes=1 ) 
  { 
    if( yes ) _writeResult=true; 
    else      _writeResult=false; 
  }

  TString result(){
    // special implementation for ATLAS/CMS paper

    char    line_[512];
    TString str_;
    TString pmstr_(" #pm ");
    TString format_("%-4.");
    format_ += _ndigit;
    format_ += "f";
    TString  valstr_;
    TString totstr_;
    TString statstr_;
    TString syststr_;
    TString theostr_;
    TString lumistr_;
    sprintf( line_, format_.Data(), _val );
    valstr_ = line_;
    str_ = valstr_;

    sprintf( line_, format_.Data(), _tot.lo );
    totstr_ = line_;
    str_ = str_ + pmstr_;
    str_ += totstr_;

    str_ += " (";

    sprintf( line_, format_.Data(), _stat.lo );
    statstr_ = line_;
    str_ = str_ + pmstr_;

    sprintf( line_, format_.Data(), _syst.lo );
    syststr_ = line_;
    str_ = str_ + pmstr_;
    str_ += syststr_;

    str_ += " )";
    str_ += " ";

    str_ += _unit;

    return str_;
  }


  void draw(){
    double  xval_[1];
    double  yval_[1];
    double    ey_[1];
    double etotlo_[1];
    double etothi_[1];
    
    xval_[0] = _val;
    yval_[0] = _y1;
    ey_[0] = 0;
    etotlo_[0] = _tot.lo;
    etothi_[0] = _tot.hi;
    
    double x0_, y0_, x1_, y1_;
    
    TBox box_;
    
    if( _stat.ok() ){
      x0_ = xval_[0]-_stat.lo;
      x1_ = xval_[0]+_stat.hi;
      y0_ = yval_[0]-_boxSizeStat;
      y1_ = yval_[0]+_boxSizeStat;
      box_.SetFillStyle(1001);
      box_.SetFillColor(kYellow-10);
      box_.DrawBox(x0_, y0_, x1_, y1_ );
      box_.SetFillStyle(0);
      box_.SetLineColor(kOrange-3);
      box_.SetLineWidth(_boxLineWidth);
      box_.DrawBox(x0_, y0_, x1_, y1_ );
    }
    
    if( _syst.ok() ){
      x0_ = xval_[0]-_syst.lo;
      x1_ = xval_[0]+_syst.hi;
      y0_ = yval_[0]-_boxSizeSyst;
      y1_ = yval_[0]+_boxSizeSyst;
      
      box_.SetFillStyle(1001);
      box_.SetFillColor(kBlue-9);
      box_.DrawBox(x0_, y0_, x1_, y1_ );
      box_.SetFillStyle(0);
      box_.SetLineColor(kBlue);
      box_.SetLineWidth(_boxLineWidth);
      box_.DrawBox(x0_, y0_, x1_, y1_ );
    }
    
    if( _theo.ok() ){
      x0_ = xval_[0]-_theo.lo;
      x1_ = xval_[0]+_theo.hi;
      y0_ = yval_[0]-_boxSizeStat;
      y1_ = yval_[0]+_boxSizeStat;
      
      box_.SetFillStyle(1001);
      box_.SetFillColor(kGray);
      box_.DrawBox(x0_, y0_, x1_, y1_ );
      
      etotlo_[0] = _theo.lo;
      etothi_[0] = _theo.hi;
      
      TLine* l1=new TLine(x0_,yval_[0],x1_,yval_[0]);
      l1->SetLineWidth(2);
      l1->SetLineColor(2);
      l1->SetLineStyle(1);
      l1->Draw();
    }
    if( _tot.ok() ){
      TGraphAsymmErrors gstat_( 1, xval_, yval_, etotlo_, etothi_, ey_, ey_ );
      gstat_.SetMarkerSize( 0 );
      gstat_.SetLineColor( kBlack );
      gstat_.SetLineWidth( _lineWidth );
      gstat_.DrawClone("Same");
    }
    
    TMarker marker_;
    marker_.SetMarkerStyle( _marker );
    marker_.SetMarkerSize( _size );
    marker_.SetMarkerColor( _color );
    marker_.DrawMarker( _val, _y1 );
    
    TString str_ = result();
    
    TLatex latex;
    // TMathText latex;
    latex.SetTextFont(_txtfont);
    latex.SetTextSize(_txtsize);
    latex.SetTextAlign(_txtalign);
    latex.DrawLatex(_x1,_y1+_dy,_name);
    // latex.DrawMathText(_x1,_y1+_dy,_name);
    if( _writeResult ){
      int sng_ = +1;
      if( _y2==0 )
	{
	  _x2 = _x1;
	  _y2 = _y1;
	  sng_ = -1;
	}
      latex.SetTextSize(_scaleResult*_txtsize);
      latex.SetTextAlign(_txtalign);
      latex.SetTextFont(_fontResult);
      latex.SetTextColor(_colorResult);
      latex.DrawLatex(_x2,_y2+sng_*_dy,str_);
      // latex.DrawMathText(_x2,_y2+sng_*_dy,str_);
    }
    
    if( _mathtext!=0 )
      {
	TMathText math;
	math.SetTextSize( _txtsize);
	math.SetTextAlign(_txtalign);
	math.DrawMathText(_x3,_y3,_mathtext);
      }
  }
  
  //  ClassDef(LEPEntry,1);
};
