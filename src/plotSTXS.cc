#include "LEPEntry.hh"
#include "TArrow.h"
#include "TLatex.h"
#include "TLine.h"
#include "TPad.h"
#include "TPaveText.h"
#include "TString.h"

using namespace std;

#include <fstream>
#include <iostream>
#include <sstream>
#include <string> 
#include <vector>

const double epsilon = 1e-7;

std::string luminosity;
double xmin;
double xmax;

std::vector<std::string> bin_name;
std::vector<std::string> file_name;
std::vector<std::string> stat_name;
std::vector<std::string> thy_name;
std::vector<bool> is_box;

bool has_comb;
std::string file_comb;
std::string stat_comb;
std::string thy_comb;

// Split a line by delimiter
vector<string> split (string s, string delimiter) {
  size_t pos_start = 0, pos_end, delim_len = delimiter.length();
  string token;
  vector<string> res;

  while ((pos_end = s.find (delimiter, pos_start)) != string::npos) {
    token = s.substr (pos_start, pos_end - pos_start);
    pos_start = pos_end + delim_len;
    res.push_back (token);
  }

  res.push_back (s.substr (pos_start));
  return res;
}

void SetAtlasStyle(){

  // use plain black on white colors                                                                                        
  Int_t icol=0; // WHITE                                                                                                    
  gStyle->SetFrameBorderMode(icol);
  gStyle->SetFrameFillColor(icol);
  gStyle->SetCanvasBorderMode(icol);
  gStyle->SetCanvasColor(icol);
  gStyle->SetPadBorderMode(icol);
  gStyle->SetPadColor(icol);
  gStyle->SetStatColor(icol);

  // set the paper & margin sizes                                                                                           
  gStyle->SetPaperSize(20,26);

  // set margin sizes                                                                                                       
  gStyle->SetPadTopMargin(0.05);
  gStyle->SetPadRightMargin(0.05);
  gStyle->SetPadBottomMargin(0.16);
  gStyle->SetPadLeftMargin(0.16);

  // set title offsets (for axis label)                                                                                     
  gStyle->SetTitleXOffset(1.1);
  gStyle->SetTitleYOffset(1.3);

  // use large fonts                                                                                                        
  int font=42; // Helvetica                                                                                               
  double tsize=0.05; // originally 0.05                                                                                   
  gStyle->SetTextFont(font);

  gStyle->SetTextSize(tsize);
  gStyle->SetLabelFont(font,"x");
  gStyle->SetTitleFont(font,"x");
  gStyle->SetLabelFont(font,"y");
  gStyle->SetTitleFont(font,"y");
  gStyle->SetLabelFont(font,"z");
  gStyle->SetTitleFont(font,"z");

  // use bold lines and markers                                                                                             
  gStyle->SetMarkerStyle(20);
  gStyle->SetMarkerSize(1.2);
  gStyle->SetHistLineWidth((Width_t)3.0);
  gStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes                                                         

  // get rid of error bar caps                                                                                              
  gStyle->SetEndErrorSize(0.);

  // do not display any of the standard histogram decorations                                                               
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots                                                                                 
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);

  gStyle->SetEndErrorSize(8);

  std::cout << "\nApplying ATLAS style settings...\n" << std::endl ;

  return;
}

TPaveText* CreatePaveText(double x1,double y1,double x2,double y2,vector<string> text,double textsize){

  TPaveText *tex=new TPaveText();

  tex->SetFillColor(0);tex->SetTextSize(0.05);
  tex->SetFillStyle(0);tex->SetBorderSize(0);
  int n=text.size();
  for(int i=0;i<n;i++) tex->AddText(text[i].c_str());
  tex->SetX1NDC(x1);
  tex->SetY1NDC(y1);
  tex->SetX2NDC(x2);
  tex->SetY2NDC(y2);
  tex->SetTextSize(textsize);
  return tex;
}

const char* GetErrStr(double errLo, double errHi){
  TString error=Form("%4.2f",(errLo+errHi)/2);
  return Form(" #pm %-4s",error.Data());
}

const char* GetErrStrAsym(double errLo, double errHi){
  int errLo1 = round(errLo*100);
  int errHi1 = round(errHi*100);
  if( errLo1 != errHi1 )
    return Form("  ^{%-4.2f}_{%-4.2f}  ",errHi,errLo);
  else
    return Form(" %-4.2f ",errHi);
}

// Get input parameters from config.txt and set global vars
void get_params(){

  std::map<string,string> params;
  std::string line;
  
  has_comb = false;

  // Read necessary input parameters from text file
  std::ifstream inputFile("config.txt");
  while(getline(inputFile, line)) {

    if (!line.length() || line[0] == '#')
      continue;

    std::vector<string> vec = split(line,",");

    if( vec.at(0) == "lumi" ){
      luminosity = vec.at(1) +  ", " + vec.at(2);
    }    
    if( vec.at(0) == "xmin" ){
      xmin = stod(vec.at(1));
    }
    if( vec.at(0) == "xmax" ){
      xmax = stod(vec.at(1));
    }
    if( vec.at(0) == "bin"){
      bin_name.push_back(vec.at(1));
      file_name.push_back(vec.at(2));
      stat_name.push_back(vec.at(3));
      thy_name.push_back(vec.at(4));
      is_box.push_back(true);
    }
    if( vec.at(0) == "lim"){
      bin_name.push_back(vec.at(1));
      file_name.push_back(vec.at(2));
      stat_name.push_back(vec.at(3));
      thy_name.push_back(vec.at(4));
      is_box.push_back(false);
    }
    if( vec.at(0) == "comb"){
      has_comb = true;
      bin_name.push_back(vec.at(1));
      file_name.push_back(vec.at(2));
      stat_name.push_back(vec.at(3));
      thy_name.push_back(vec.at(4));
      is_box.push_back(true);
    }
  }
  inputFile.close();
}

int main(){

  SetAtlasStyle();

  get_params();
  
  std::string outputDir="fig-STXS";
  system(("mkdir -vp "+outputDir).c_str());
  std::vector<double> mean;
  std::vector<double> err_lo,err_lo_thy,err_lo_sys,err_lo_stat;
  std::vector<double> err_hi,err_hi_thy,err_hi_sys,err_hi_stat;
  std::vector<TString> name;
  std::vector<TString> name2,tag;
  
  std::vector<TString> inputs;
  std::vector<bool> cutoff;
  
  const int nBins = bin_name.size();
  std::string fileName[nBins];
  std::string fileName_stat[nBins];
  std::string fileName_thy[nBins];
  std::string displayName[nBins];

  // Loop over bins
  for(int i=0; i<nBins; i++){
    fileName[i] = file_name.at(i);
    fileName_stat[i] = stat_name.at(i);
    fileName_thy[i] = thy_name.at(i);
    displayName[i] = bin_name.at(i);
    
    std::cout << displayName[i] << std::endl;

    name.push_back(displayName[i]);
    inputs.push_back(displayName[i]);
    tag.push_back("sigmaH"); 
    cutoff.push_back(false);
    
    std::string line;
    std::string a,b,c,d,e,f;
    std::string x,y,z;
    
    // Read in central value and total uncertainty
    std::ifstream input(fileName[i].c_str());
    while(getline(input, line)) {
      std::istringstream iss(line);
      iss>>a>>b>>c;
      break;
    }
    mean.push_back(stod(a));
    err_hi.push_back(stod(c));
    err_lo.push_back(stod(b));

    // Read in theory uncertainty
    std::ifstream input_thy(fileName_thy[i].c_str());
    while(getline(input_thy, line)) {
      std::istringstream iss_thy(line);
      iss_thy>>x>>y>>z;
      break;
    }
    err_hi_thy.push_back(stod(z));
    err_lo_thy.push_back(stod(y));
    
    if( is_box.at(i) ){
      // Read in stat only error
      std::ifstream input_stat(fileName_stat[i].c_str());
      while(getline(input_stat, line)) {
	std::istringstream iss_stat(line);
	iss_stat>>d>>e>>f;
	break;
      }
      
      err_hi_stat.push_back(stod(f));
      err_lo_stat.push_back(stod(e));
      err_hi_sys.push_back(sqrt(stod(c)*stod(c)-stod(f)*stod(f)));
      err_lo_sys.push_back(sqrt(stod(b)*stod(b)-stod(e)*stod(e)));

    }
    else{
      // Set stat and syst to 0
      err_hi_stat.push_back(0);
      err_lo_stat.push_back(0);
      err_hi_sys.push_back(0);
      err_lo_sys.push_back(0);
    }
  }

  int nslice = nBins;
  
  double txtsize_(0.035);
  int    align_(12);
  double markerSize_(1.2);
  double lineWidth_(2.5);
  double boxLineWidth_(1.2);
  double statBoxSize_(0.24);
  double systBoxSize_(0.16);
  
  statBoxSize_=0.18;
  systBoxSize_=0.13;
  
  vector<TString> results;
  
  // Create anvas
  TCanvas *c = new TCanvas("summary","summary",800,600);
  TPad *pad1 =  new TPad("pad1","pad1name",0.,0.,1.,1.);
  pad1->SetLeftMargin(0.05);
  pad1->Draw();
  pad1->cd();
  
  // Draw dummy histogram to set plot limits
  TH1F* h0 = new TH1F("h0","h0",10000,xmin,xmax);
  // h0->SetMinimum(0.001);
  h0->SetMinimum(0.001);
  h0->SetLineWidth(0);
  h0->SetLineColor(0);
  h0->SetMaximum(nslice+2);
  h0->SetMinimum(-0.5);
  h0->GetXaxis()->SetTitle("#sigma/#sigma^{SM}");
  h0->GetXaxis()->SetTitleSize(0.055);
  h0->GetYaxis()->SetLabelOffset(1);
  h0->GetXaxis()->SetLabelSize(0.055);
  h0->GetYaxis()->SetNdivisions(1);
  h0->SetLineWidth(0);
  h0->Draw();

  // Draw theory error (bins and lims)
  double muSM = 1;
  double muErrLo = 0;
  double muErrHi = 0;
  
  for(int islice=0;islice<nslice;islice++){
    muErrLo = err_lo_thy.at(islice);
    muErrHi = err_hi_thy.at(islice);

    TBox *box = new TBox (muSM-muErrLo, islice+0.1, muSM+muErrHi, islice+0.9);
    box->SetFillStyle(1001);
    box->SetFillColor(kGray);
    box->SetLineColor(kGray);
    box->SetLineWidth(0);
    box->Draw("l");
  }  
  
  // Draw Standard Model line
  TLine *lband_=new TLine(muSM, 0., muSM, nslice);
  lband_->SetLineWidth(2);
  lband_->SetLineColor(2);
  lband_->SetLineStyle(1);
  lband_->Draw("same");
  
  // Draw LEPEntry or limit arrow for each bin
  for(int islice=0; islice<nBins; islice++){

    TString printStr=Form("%30s", name[islice].Data());

    if( is_box.at(islice) ){
      
      printStr+=Form("\t %.3f \t +%.3f \t -%.3f \t +%.3f \t -%.3f \t +%.3f \t -%.3f", mean[islice],err_hi[islice],err_lo[islice],err_hi_stat[islice],err_lo_stat[islice],err_hi_sys[islice],err_lo_sys[islice]);
      TString central=Form("%6.2f", mean[islice]);
      printStr=Form("%-6s #pm%s ( #pm%s, #pm%s)", central.Data(), GetErrStrAsym(err_lo[islice], err_hi[islice]), GetErrStrAsym(err_lo_stat[islice], err_hi_stat[islice]), GetErrStrAsym(err_lo_sys[islice], err_hi_sys[islice]));
      
      results.push_back(printStr);
      
      cout<<printStr<<endl;
      
      LEPEntry entry( "", "", "" );
      entry.setStatName("");
      entry.setVal(  mean[islice] );
      entry.setAsymmTot( err_lo[islice], err_hi[islice] );
      entry.setAsymmSyst( err_lo_sys[islice], err_hi_sys[islice] );
      entry.setAsymmStat( err_lo_stat[islice], err_hi_stat[islice] );
      entry.setNdigit( 2 );      
      entry.setStyle( kFullCircle, markerSize_, kBlack, 42, txtsize_, align_, lineWidth_ );
      entry.setX( xmin, xmax );      
      entry.setY( islice+0.5, islice+0.5 ); 
      entry.setScale(0 );
      entry.writeResult(1);
    
      entry.setBoxLineWidth(boxLineWidth_);
      entry.setStatBoxSize(statBoxSize_);
      entry.setSystBoxSize(systBoxSize_);
      
      entry.draw();
    }
    else{
      printStr=Form("\t < %.3f (68%% CL)",mean[islice]+err_hi[islice]);

      results.push_back(printStr);

      cout<<printStr<<endl;

      TArrow* a = new TArrow(0,islice+0.5,err_hi[islice],islice+0.5,0.01,"<|");
      a->SetLineWidth(2.5);
      a->SetLineColor(kBlack);
      a->Draw();
      TLine* linea = new TLine( 0, islice+0.35, 0, islice+0.65 );
      linea->SetLineWidth(2.5);
      linea->SetLineColor(kBlack);
      linea->Draw();
    }
  }

  TLine *comb = new TLine(xmin,1,xmax,1);
  comb->SetLineWidth(2);
  comb->SetLineColor(kBlack);
  comb->SetLineStyle(2);
  if( has_comb ) comb->Draw("same");

  TLatex la; //l.SetTextAlign(12); l.SetTextSize(tsize);
  la.SetNDC();
  la.SetTextFont(72);
  la.SetTextColor(1);
  la.SetTextSize(0.035);
  
  TLatex lp;
  lp.SetNDC();
  lp.SetTextFont(42);
  lp.SetTextColor(1);
  lp.SetTextSize(0.035);
  
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextColor(1);
  l.SetTextSize(0.035);
  /*  
  TLine* linex[100];
  for(int i=0; i<int(mean.size()); i++){
    if( cutoff.at(i)==1 ){
      linex[i] = new TLine( mean.at(i)-err_lo.at(i), i,  mean.at(i)-err_lo.at(i), i+1 );
      linex[i] -> SetLineWidth(2);
      linex[i] -> SetLineColor(kBlack);
      linex[i] -> SetLineStyle(kDashed);
      linex[i] -> Draw("same");
    }
  }
  */
  double x1=0.05,y1=0.95,x2=0.16,y2=0.95;
  
  vector<string> pavetext1,pavetext2;
  pavetext2.push_back("#bf{#it{ATLAS}} Internal");
  pavetext2.push_back(luminosity);
  
  TPaveText* text1=CreatePaveText(x2-0.5,y1+0.01,x2-0.01,y1+0.18,pavetext1,0.04);
  TPaveText* text2=CreatePaveText(x1+0.03,y2-0.18,x1+0.4,y2-0.05,pavetext2,0.04);
  
  LEPEntry totleg("Total");
  totleg.setStyle( kFullCircle, markerSize_, kBlack, 42, txtsize_, align_, lineWidth_ );
  LEPEntry systleg("Syst.");
  systleg.setStyle( kFullCircle, 0, kBlack, 42, txtsize_, align_, lineWidth_ );
  systleg.setSystBoxSize(systBoxSize_);
  systleg.setBoxLineWidth(boxLineWidth_);
  LEPEntry statleg("Stat.");
  statleg.setStyle( kFullCircle, 0, kBlack, 42, txtsize_, align_, lineWidth_ );
  statleg.setStatBoxSize(statBoxSize_);
  statleg.setBoxLineWidth(boxLineWidth_);
  LEPEntry smleg("SM");
  smleg.setStyle( kFullCircle, 0, kBlack, 42, txtsize_, align_, lineWidth_ );
  
  float xleg_, yleg_, eleg_, tleg_;
  yleg_ = nslice+1.2;
  xleg_ = xmax-3.5;   
  eleg_ = 0.15;  
  tleg_ = xleg_+0.25;
  
  totleg.setVal( xleg_ );
  totleg.setTot( eleg_ );
  
  totleg.setX( tleg_ );
  totleg.setY( yleg_ ); 
  totleg.draw();
  
  xleg_ += 1;
  
  tleg_ = xleg_+0.25;
  
  statleg.setVal( xleg_ );
  statleg.setStat( 1.0*eleg_ );
  
  statleg.setX( tleg_ );
  statleg.setY( yleg_ );
  statleg.draw();
  
  xleg_ += .9;
  
  tleg_ = xleg_+0.2;
  
  systleg.setVal( xleg_ );
  systleg.setSyst( 0.75*eleg_ );
  
  systleg.setX( tleg_ );
  systleg.setY( yleg_ );
  systleg.draw();
  
  xleg_ += .9;
  
  tleg_ = xleg_+0.2;
  
  smleg.setVal( xleg_ );
  smleg.setTheo( 0.75*eleg_ );
  
  smleg.setX( tleg_ );
  smleg.setY( yleg_ );
  smleg.draw();
  
  text1->Draw("same");
  text2->Draw("same");
  
  pad1->cd();
  TLatex lx; 
  lx.SetNDC();
  lx.SetTextColor(1);
  lx.SetTextSize(0.035);
  
  // Draw text
  lx.DrawLatex(0.61,0.11+(nslice+1.0)*0.79/(double)(nslice+2), Form("             Total      Stat.   Syst."));
  for(int i=0; i<nslice; i++){    
    lx.SetTextSize(0.035);
    lx.DrawLatex(0.07,0.185+(i+0.4)*0.76/(double)(nslice+2), Form("%s",name.at(i).Data()));
    
    lx.SetTextSize(0.035);
    lx.DrawLatex(0.61,0.185+(i+0.4)*0.76/(double)(nslice+2), Form("%s",results.at(i).Data()));
  }
  
  pad1->RedrawAxis();
  pad1->Update();
  pad1->SaveAs((outputDir+"/STXS.png").c_str());

  return 0;
}
